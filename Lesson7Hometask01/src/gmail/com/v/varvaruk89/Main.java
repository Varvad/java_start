package gmail.com.v.varvaruk89;

import java.util.Calendar;
import java.util.Date;

public class Main {

	public static void main(String[] args) {
		Calendar cl = Calendar.getInstance();
		Date date = cl.getTime();
		Calendar beforeCl = Calendar.getInstance();
		beforeCl.set(Calendar.MONTH, Calendar.MONTH - 1);
		Date beforeDate = beforeCl.getTime();
		System.out.println(date);
		System.out.println(beforeDate);
		long millis;

		millis = date.getTime() - beforeDate.getTime();
		System.out.println(millis);
	}

}
