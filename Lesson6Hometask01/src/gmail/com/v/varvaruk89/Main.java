package gmail.com.v.varvaruk89;

import java.util.Arrays;

public class Main {

	public static void main(String[] args) {

		int[] array = new int[20];
		input(array);
		System.out.println("Array=" + Arrays.toString(array));
		int max;
		max = max(array);
		System.out.println("Max=" + max);
	}

	static void input(int[] array) {
		for (int i = 0; i < array.length; i++) {

			array[i] = (int) (Math.random() * 1000);
		}

	}

	static int max(int... array) {
		int max = array[0];
		for (int i = 0; i < array.length; i++) {
			if (max < array[i]) {
				max = array[i];
			}
		}

		return max;
	}
}
