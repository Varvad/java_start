package com.gmail.v.varvaruk89;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int size;
		System.out.println("Input the size and press enter.");
		size = sc.nextInt();
		int[] array = new int[size];
		System.out.println("Input array elements");
		for (int i = 0; i < size; i++) {
			array[i] = sc.nextInt();
		}
		System.out.print("Array =");
		for (int i = 0; i < size; i++) {
			System.out.print(" " + array[i]);
		}
		sc.close();
	}

}
