package com.gmail.v.varvaruk89;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		double ax;
		double ay;
		double bx;
		double by;
		double cx;
		double cy;
		double pointX;
		double pointY;
		double a;
		double b;
		double c;

		System.out.println("Please input Point X  and press enter.");
		pointX = sc.nextDouble();

		System.out.println("Please input Point Y  and press enter.");
		pointY = sc.nextDouble();

		ax = 0.0;
		ay = 0.0;
		bx = 4.0;
		by = 4.0;
		cx = 6.0;
		cy = 1.0;

		a = (ax - pointX) * (by - ay) - (bx - ax) * (ay - pointY);
		b = (bx - pointX) * (cy - by) - (cx - bx) * (by - pointY);
		c = (cx - pointX) * (ay - cy) - (ax - cx) * (cy - pointY);

		if ((a >= 0 && b >= 0 && c >= 0) || (a <= 0 && b <= 0 && c <= 0)) {
			System.out.println("Point in triangle.");
		} else {
			System.out.println("Point is not in a triangle.");
		}
		sc.close();

	}

}
