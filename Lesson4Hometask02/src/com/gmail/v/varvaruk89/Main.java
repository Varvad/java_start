package com.gmail.v.varvaruk89;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		int n;
		int factorial = 1;
		Scanner sc = new Scanner(System.in);
		System.out.println("Please input n (4<n<16) and press enter.");
		n = sc.nextInt();
		if (n > 4 & n < 16) {

			for (; n > 0; n--) {

				factorial = (factorial * n);

			}
			System.out.println(factorial);

		} else {
			System.out.println("Are you serious??? N out of range.Please input n (4<n<16).");
		}
		sc.close();
	}

}
