package gmail.v.varvaruk89;

import java.util.Arrays;

public class Main {

	public static void main(String[] args) {

		int[] array = new int[4];
		input(array);
		System.out.println("Array=" + Arrays.toString(array));
		int element = 5;
		System.out.println("indexElementa "+element+" = "+indexElementa(array, element));
	}

	static void input(int[] array) {
		for (int i = 0; i < array.length; i++) {

			array[i] = (int) (Math.random() * 10);
		}

	}

	static int indexElementa(int[] array, int element) {
		int index = -1;
		for (int i = 0; i < array.length; i++) {
			if (array[i] == element) {
				index = i;
			}
		}
		return index;
	}
}
