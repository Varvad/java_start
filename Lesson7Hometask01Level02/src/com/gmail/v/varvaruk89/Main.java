package com.gmail.v.varvaruk89;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Input date format - dd:MM:yyyy and press enter.");
		Date dateInput = inputData(sc.nextLine());
		Calendar clInput = Calendar.getInstance();
		Calendar cl = Calendar.getInstance();
		clInput.setTime(dateInput);
		int[] difference = difference(clInput, cl);
		outputDifference(difference);
		sc.close();
	}

	static Date inputData(String datText) {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd:MM:yyyy");
		try {
			date = sdf.parse(datText);
		} catch (Exception e) {
			System.out.println("Input date format - dd:MM:yyyy!");
			

		}
		return date;
	}

	static int[] difference(Calendar clInput, Calendar cl) {
		// long diff = Math.abs(cl.getTimeInMillis() -
		// clInput.getTimeInMillis());

		int[] difference = new int[3];

		difference[2] = Math.abs(cl.get(Calendar.YEAR) - clInput.get(Calendar.YEAR));
		difference[1] = cl.get(Calendar.MONTH) - clInput.get(Calendar.MONTH);
		difference[0] = cl.get(Calendar.DATE) - clInput.get(Calendar.DATE);
		return difference;
	}

	static void outputDifference(int... difference) {
		if (difference[0] == 0 & difference[1] == 0 & difference[2] == 0) {
			System.out.println("All is well man. There are no differences.");
		} else {
			System.out.print("Difference in values: ");
			if (difference[0] != 0) {
				System.out.print(Math.abs(difference[0]) + " days ");
			}
			if (difference[1] != 0) {
				System.out.print(Math.abs(difference[1]) + " month ");
			}
			if (difference[2] != 0) {
				System.out.print(Math.abs(difference[2]) + " Year.");
			}

		}
	}
}
