package com.gmail.v.varvaruk89;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String money;
		int centsInt = 0;
		System.out.println("How much money do you have?");
		money = sc.nextLine();
		String[] moneyString = money.split("[,]");

		System.out.println();
		int dolarsInt = Integer.parseInt(moneyString[0]);

		if (dolarsInt >= 1000000000) {
			System.out.println("Error dolarsInt>=1000000000");
			dolarsInt = 0;

		}

		if (moneyString.length == 2) {

			if (moneyString[1].length() < 2) {
				centsInt = 10 * Integer.parseInt(moneyString[1]);
			} else {
				centsInt = Integer.parseInt(moneyString[1]);
			}

		}

		int[] dollarsArray = new int[3];

		dollarsArray[0] = (int) dolarsInt / 1000000;
		dollarsArray[1] = (int) (dolarsInt - dollarsArray[0] * 1000000) / 1000;
		dollarsArray[2] = (int) (dolarsInt - dollarsArray[0] * 1000000 - dollarsArray[1] * 1000);

		String[] dozens = { "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };
		String[] tens = { "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen",
				"eighteen", "nineteen" };
		String[] numbers = { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };

		System.out.print("You have: ");

		for (int i = 0; i < dollarsArray.length; i++) {

			if (dollarsArray[i] > 0) {
				dolarsInt = dollarsArray[i];

			} else {
				continue;
			}

			if (dolarsInt >= 1000 & dolarsInt < 1000000) {

				dolarsInt = dolarsInt / 1000;
			}

			if (dolarsInt >= 100 & dolarsInt < 1000) {
				System.out.print(numbers[dolarsInt / 100] + " hundred ");
				dolarsInt = dolarsInt % 100;
			}
			if (dolarsInt >= 20 & dolarsInt < 100) {
				System.out.print(dozens[dolarsInt / 10 - 2] + " ");
				dolarsInt = dolarsInt % 10;
			}
			if (dolarsInt >= 10 & dolarsInt <= 19) {
				System.out.print(tens[dolarsInt % 10] + " ");
			}
			if (dolarsInt < 10) {
				System.out.print(numbers[dolarsInt] + " ");
			}

			if (dollarsArray[i] > 0 & i == 0) {
				System.out.print(" milion ");
			}

			if (dollarsArray[i] > 0 & i == 1) {
				System.out.print(" thousand ");
			}
		}
		System.out.print(" dollars, ");

		if (centsInt >= 20 & centsInt < 100) {
			System.out.print(dozens[centsInt / 10 - 2] + " ");
			centsInt = centsInt % 10;
		}
		if (centsInt >= 10 & centsInt <= 19) {
			System.out.print(tens[centsInt % 10] + " ");
		}
		if (centsInt < 10) {
			System.out.print(numbers[centsInt] + " ");
		}
		System.out.print(" cents. ");
		sc.close();
	}

}
