package com.gmail.v.varvaruk89;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int height;
		int width;
		System.out.println("Please input height (height>0) and press enter.");
		height = sc.nextInt();

		System.out.println("Please input width (width>0) and press enter.");

		width = sc.nextInt();

		for (int i = 1; i <= height; i++) {

			for (int j = 1; j <= width; j++) {

				if (i == 1 | i == height) {
					System.out.print("*");
				} else if (j == 1 | j == width) {
					System.out.print("*");
				} else {
					System.out.print(" ");
				}

			}
			System.out.println("");
		}
		sc.close();
	}

}
