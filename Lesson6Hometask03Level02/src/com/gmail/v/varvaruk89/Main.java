package com.gmail.v.varvaruk89;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Input length arrays");
		int n = sc.nextInt();
		int[] array = inputArrey(n);
		System.out.println("");

		outCombo(array, 0);
		sc.close();
	}

	static int[] inputArrey(int a) {
		int[] array = new int[a];
		for (int i = 0; i < a; i++) {
			array[i] = (int) (Math.random() * 100);
			System.out.print(array[i]+" ");
		}

		return array;
	}

	public static void permute(int[] array) {
		outCombo(array, 0);
	}

	static void outCombo(int[] array, int index) {
		if (index >= array.length - 1) {
			for (int i = 0; i < array.length - 1; i++) {
				System.out.print(array[i] + "/");
			}
			if (array.length > 0)
				System.out.print(array[array.length - 1]);
			System.out.println("");
			return;
		}

		for (int i = index; i < array.length; i++) {
			int t = array[index];
			array[index] = array[i];
			array[i] = t;
			outCombo(array, index + 1);
			t = array[index];
			array[index] = array[i];
			array[i] = t;
		}
	}
}