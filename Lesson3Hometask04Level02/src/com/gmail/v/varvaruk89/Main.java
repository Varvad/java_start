package com.gmail.v.varvaruk89;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int a;
		int b;
		int c;
		int d;
		int e;
		int f;
		int x;

		System.out.println("Please input number  and press enter!");
		x = sc.nextInt();
		a = x / 100000;
		b = x % 100000 / 10000;
		c = x % 100000 % 10000 / 1000;
		d = x % 100000 % 10000 % 1000 / 100;
		e = x % 100000 % 10000 % 1000 % 100 / 10;
		f = x % 100000 % 10000 % 1000 % 100 % 10 / 1;

		if (a == f && b == e && c == d) {

			System.out.println("The number is a palindrome.");
		} else {
			System.out.println("The number is not a palindrome.");
		}
		sc.close();
	}
}
