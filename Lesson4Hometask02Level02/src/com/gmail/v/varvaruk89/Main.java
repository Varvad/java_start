package com.gmail.v.varvaruk89;

public class Main {

	public static void main(String[] args) {

		for (int j = 2; j <= 100; j++) {
			boolean test = false;

			for (int i = 2; i * i <= j; i++) {
				test = (j % i == 0);
				if (test) {
					break;

				}

			}
			if (!test) {
				System.out.print(j+" ");
			}
		}

	}
}
