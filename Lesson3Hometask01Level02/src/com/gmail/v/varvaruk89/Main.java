package com.gmail.v.varvaruk89;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		double r;
		double pointX;
		double pointY;
		double test;
		r = 4.0;

		System.out.println("Please input Point X  and press enter.");
		pointX = sc.nextDouble();
		System.out.println("Please input Point Y  and press enter.");
		pointY = sc.nextDouble();

		test = Math.sqrt((pointX * pointX) + (pointY * pointY));

		if (test <= r) {
			System.out.println("Point in a circle.");

		} else {
			System.out.println("The point is not in a circle.");
		}

		sc.close();

	}

}
