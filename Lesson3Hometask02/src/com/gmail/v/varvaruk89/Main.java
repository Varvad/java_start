package com.gmail.v.varvaruk89;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int numberApartment;
		int floor;
		int entrance;
		int test;

		System.out.println("Please input apartment number and press enter!");

		numberApartment = sc.nextInt();
		test = numberApartment % 4;

		if (numberApartment >= 1 && numberApartment <= 144 && test != 0) {
			entrance = (numberApartment / 36) + 1;

			floor = ((((numberApartment - ((numberApartment/36)*36)))/4)+1)  ;

			System.out.println("Apartment on the " + floor + "st floor of " + entrance + " entrance.");

		} else if ((numberApartment >= 1 && numberApartment <= 144 && test == 0)) {

			entrance = (numberApartment / 37) + 1;

			floor = (((numberApartment - ((numberApartment/37)*36)))/4);

			System.out.println("Apartment on the " + floor + "st floor of " + entrance + " entrance.");
		} else {

			System.out.println("There is no such apartment in the house!");
		}
		sc.close();

	}

}

