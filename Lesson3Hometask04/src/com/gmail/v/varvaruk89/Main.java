package com.gmail.v.varvaruk89;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		double a;
		double b;
		double c;
		Scanner sc = new Scanner(System.in);
		System.out.println("Please input A and press enter");
		a = sc.nextDouble();
		System.out.println("Please input B and press enter");
		b = sc.nextDouble();
		System.out.println("Please input C and press enter");
		c = sc.nextDouble();
		if (a >= b + c || b >= a + c || c >= a + b) {
			System.out.print("Triangle does not exist.");
		} else {
			System.out.print("Triangle exist.");
		}
		sc.close();
	}

}
