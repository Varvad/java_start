package com.gmail.v.varvaruk;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double radius;
		double length;
		System.out.println("Please input Radius and press enter!");
		radius = sc.nextDouble();
		length = Math.PI*(radius*2);
		System.out.println("Length="+length);
		sc.close();
	}

}
