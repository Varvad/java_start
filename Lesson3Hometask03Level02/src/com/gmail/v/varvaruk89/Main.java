package com.gmail.v.varvaruk89;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int a;
		int b;
		int c;
		int d;
		int n;
		int testA;
		int testB;

		System.out.println("Please input number of ticket and press enter!");
		n = sc.nextInt();
		a = n / 1000;
		b = n % 1000 / 100;
		c = n % 1000 % 100 / 10;
		d = n % 1000 % 100 % 10 / 1;

		testA = a + b;
		testB = c + d;

		if (testA >= 10) {
			a = testA / 10;
			b = testA % 10;
			testA = a + b;
		}
		if (testB >= 10) {
			c = testB / 10;
			d = testB % 10;
			testB = c + d;
		}

		if (testA == testB) {
			System.out.println("Happy ticket!!!");
		} else {
			System.out.println("The ticket is unlucky. And you're a loser!");
		}

		sc.close();
	}

}
