package com.gmail.v.varvaruk89;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Please input sequence of numbers and press enter");
		String text = sc.nextLine();
		String array[] = text.split("[\\s,.!]+");
		int[] arrayint = stringToInt(array);
		System.out.println(Arrays.toString(arrayint));
		int element = nextElement(arrayint);

		if (element != 0) {

			System.out.println("Next element=" + nextElement(arrayint));
		} else {
			System.out.println("I don't know such a sequence.");
		}

		sc.close();
	}

	static int[] stringToInt(String... array) {
		int[] arrayint = new int[array.length];
		for (int i = 0; i < array.length; i++) {
			arrayint[i] = Integer.parseInt(array[i]);
		}
		return arrayint;
	}

	static int nextElement(int... array) {
		int test = 0;
		boolean alarm = true;
		if (array[1] - array[0] == array[array.length - 1] - array[array.length - 2]) {
			alarm = testElements(array, (array[array.length - 1] - array[array.length - 2]), 0);
			if (alarm) {
				test = array[array.length - 1] + (array[array.length - 1] - array[array.length - 2]);
			} else {
				test = 0;
			}
		} else if (array[1] / array[0] == array[array.length - 1] / array[array.length - 2]) {

			alarm = testElements(array, (array[1] / array[0]), 1);
			if (alarm) {
				test = array[array.length - 1] * (array[1] / array[0]);
			} else {
				test = 0;
			}
		} else {
			test = nextElementInDegree(array);

		}

		return test;
	}

	static int nextElementInDegree(int... array) {
		int tes = 0;
		boolean test = true;
		for (int i = 1; i < array.length; i++) {
			for (int j = 2; j < 10; j++) {
				if ((array[0] != 0) & (int) Math.pow(i + 1, j) == array[i]) {
					test = testElements(array, j, 2);
					if(test){
					tes = (int) Math.pow(array.length + 1, j);
					}else{
						tes =0;
					}
				}
			}

		}

		return tes;
	}

	static boolean testElements(int[] array, int a, int b) {
		boolean test = true;
		if (b == 0) {
			for (int i = 0; i < array.length - 2; i++) {
				if (array[i + 1] != array[i] + a) {
					test = false;
					break;
				}
			}
		}
		if (b == 1) {
			for (int i = 0; i < array.length - 2; i++) {
				if (array[i + 1] != array[i] * a) {
					test = false;
					break;
				}
			}
		}
		if(b==2){
			for(int i =1;i<array.length;i++){
				if(array[i]!=(int) Math.pow(i + 1, a)){
					test = false;
				}
			}
		}

		return test;
	}
}
