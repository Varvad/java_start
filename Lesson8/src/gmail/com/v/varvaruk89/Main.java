package gmail.com.v.varvaruk89;

import java.io.File;
import java.io.IOException;

public class Main {

	public static void main (String [] args){
		File fileOne = new File("a.txt");
		System.out.println("file  is real "+fileOne.exists());
		
		try{
			fileOne.createNewFile();
		}catch (IOException e) {
			System.out.println(e);
			// TODO: handle exception
		}
		
		File fileTwo = new File ("b.txt");
		fileOne.renameTo(fileTwo);
		
		File folderTwo = new File("AAA");
		folderTwo.mkdirs();
		
		File  fileThree = new File (folderTwo,"x.doc");
		fileTwo.renameTo(fileThree);
		
		File  folderOne = new File(".");
		
		File [] files = folderOne.listFiles();
		
	//	fileTwo.delete();
	//	fileThree.delete();
		//folderTwo.delete();
		deletFolder(folderTwo);
		
		for (File file: files){
			System.out.println(file+ (file.isFile()?"-file":"-folder"));
		}
	
	}

	public  static void deletFolder(File folder){
		if (folder.isFile()){
			folder.delete();
		}else{
			File [] files = folder.listFiles();
			for(int i =0;i<files.length;i++){
				deletFolder(files[i]);
			}
		folder.delete();
		}
	}
	
}
