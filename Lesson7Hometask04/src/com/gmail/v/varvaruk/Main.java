package com.gmail.v.varvaruk;

import java.util.Formatter;

public class Main {

	public static void main(String[] args) {
	
		 double pi = Math.PI;
		Formatter form = new Formatter();
		for(int i=2;i<12;i++){
			String par = "%"+"."+i+"f";
			String formattedDouble = String.format(par, pi);
			System.out.println(formattedDouble);
		}
	}

}
