package gmail.com.v.varvaruk89;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the words and press enter.");
		String s = sc.nextLine();
		System.out.println(quantity(s) + " words");

		sc.close();
	}

	static int quantity(String s) {
		int num = 0;
		s = s + " ";
		num = s.split("[\\s,.!]+").length;
		return num;
	}
}
