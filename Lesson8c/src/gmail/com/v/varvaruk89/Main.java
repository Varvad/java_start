package gmail.com.v.varvaruk89;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		File file = new File("src/gmail/com/v/varvaruk89/Main.java");
		//String text = loadTextFromFile(file);
		String text = fastLoadFromFile(file);
		System.out.println(text);// TODO Auto-generated method stub

	}

	public static String loadTextFromFile(File file) {
		StringBuilder sb = new StringBuilder();
		try (Scanner sc = new Scanner(file)) {
			for (; sc.hasNextLine();) {
				sb.append(sc.nextLine());
				sb.append(System.lineSeparator());
			}
		} catch (IOException e) {
			System.out.println(e);
		}
		return sb.toString();
	}

	public static String fastLoadFromFile(File file){
		StringBuilder sb = new StringBuilder();
		try(BufferedReader br = new BufferedReader(new FileReader(file))){
			String text ="";
			for(;(text=br.readLine())!=null;){
				sb.append(text);
				sb.append(System.lineSeparator());
			}
		}catch(IOException e){
			System.out.println(e);
		}
	return sb.toString();
	}
	
	
	
	
	
}
