package com.gmail.v.varvaruk89;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int n;
		int test = 0;
		int i = 1;

		System.out.println("Please input N and press enter.");
		n = sc.nextInt();

		while (i <= n * 2 - 1) {

			System.out.print("*");

			test++;

			if (test >= i | test >= n * 2 - i) {
				System.out.println("+");

				test = 0;
				i++;
			}
			sc.close();
		}
	}
}
