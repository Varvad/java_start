package com.gmail.v.varvaruk89;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String line;
		System.out.println("Input line and press enter.");
		line = sc.nextLine();
		char[] charLine = line.toCharArray();
		int sum = 0;

		for (int i = 0; i < charLine.length; i++) {

			if (charLine[i] == 'b') {
				sum = sum + 1;
			}

		}

		System.out.println("In line "+sum+" b.");
		sc.close();
	}

}
